import os as os
import datetime as dt
from pandas import DataFrame, merge

class FileData:
    def __init__(self, file_path, exists, basename, dir_path, infos):
        self.file_path = file_path
        self.exists = exists
        self.basename = basename
        self.dir_path = dir_path
        self.unique_dirs = set(self.dir_path)
        self.infos = infos
        self.statistics = None # TODO(): Fill in.
    
    def __repr__(self):
        print("\n-- Files:\n")
        cnt = 0
        for nm in self.basename:
            print(str(cnt) + " - " + nm)
            cnt += 1
        print("\n-- Directories:\n")
        cnt = 0
        for nm in self.dir_path:
            print(str(cnt) + " - " + nm)
            cnt += 1

def execute_from_text(data, include_key=True, class_attributes=False):
    """
    Function to execute commands created from strings on the fly.
    Returns True if all execution successful.
    Input: a dictionary with the desired text to be executed.
    """
    if class_attributes == True:
        prefix = "self."
    else:
        prefix = ""

    for elt in data:
            if include_key == False:
                elt = ""
                assign = ""
            else:
                assign = " = "
            ex_str = f"{prefix}{elt}{assign}{data[elt]}"
            exec(ex_str)
    
    return True

def list_file_info(file_path):
    files = file_path

    file_path = [os.path.abspath(path) for path in files if path != None and os.path.isdir(path) == False]
    exists = [os.path.exists(path) for path in files if path != None and os.path.isdir(path) == False]
    dir_path = [os.path.dirname(path) for path in files if path != None and os.path.isdir(path) == False]
    basename = [os.path.basename(path) for path in files if path != None and os.path.isdir(path) == False]
    infos = [os.stat(path) for path in files if path != None and os.path.isdir(path) == False]
    # TODO(): datetime.datetime.utcfromtimestamp(object.st_mtime) to get datetime info
    #         from stat, then convert in proper format from there.
        
    # Convert all to tuples and make output dictionary
    file_path = tuple(file_path)
    exists = tuple(exists)
    basename = tuple(basename)
    dir_path = tuple(dir_path)
    infos = tuple(infos)

    output = FileData(file_path=file_path,
                        exists=exists,
                        basename=basename,
                        dir_path=dir_path,
                        infos=infos)

    return output
#
#    def list_dir_content(self, ext=None, all=False): #
#
#         # List all directories and files
#        list_all = [os.path.join(root, name) for root, dirs, files in os.walk(dir) for name in dirs + files]
#        
#        # Handling extensions
#        if ext != None and isinstance(ext, str):
#
#            if "." in ext:
#                ext = ext.strip(".")
#            list_files = [elt for elt in list_all if "." + ext in os.path.basename(elt)]
#
#            if len(list_files) == 0:
#                print("No files found.\n" + "Please check the 'ext' argument.\n")
#
#        elif ext != None:
#            print("Please check the 'ext' argument again.\n" + "It should be of type str.\n\n" + "Exiting now...\n")
#            exit()
#
#        # Define and return output
#        if all == True:
#            output = tuple(list_all)
#        else:
#            output = tuple(list_files)
#
#        return(output)

def get_file_information(dir_path=None, file_path=None, ext=None):
    """ Generic function to get information about files
    Returns:
        List of tuples containing file information and
        statistics about the 'question'.
    """
    if dir_path is None and file_path is None:
        print("Please enter either a directory path (or tuple of directory paths)\
                or a file path (or tuple of file paths).\nExiting now...")
        exit()
    
    if dir_path is not None and file_path is not None:
        print("Please enter either a directory path (or tuple of directory paths)\
                or a file path (or tuple of file paths).\nExiting now...")
        exit()

    if dir_path is not None:
        files = list_dir_content(dir=dir_path, ext=ext, all=False)
    
    if file_path is not None:
        files = file_path

    output = list_file_info(file_path=files)
    return output

def create_dataframes(data_list):
    # Expect the headers to be in the first row.

    output = []
    for dt in data_list:
        df = DataFrame(dt[1:])
        df.columns = dt[0]
        output.append(df)

    return(output)

def merge_excel_info(df_tuple, how = "inner", on=None, left_on = None, right_on = None):
    # The first element of the tuple is the one on the left during join
    output = merge(df_tuple[0], df_tuple[1], how = "inner", on = on)
    
    if len(df_tuple) > 2:
        for df in df_tuple[3:]:
            output = merge(output, df, how = how, on = on, left_on = left_on, right_on = right_on)
    
    return(output)

def create_file_name():
    pass

def list_dir_content(dir, ext = None, all = False):

    # clean up extension
    if ext is not None and "." in ext:
        ext = ext.strip(".")

    # List all directories and files
    if type(dir) == str:
        list_all = [os.path.join(root, name) for root, dirs, files in os.walk(dir) for name in dirs + files]
    else:
        list_all = []
        for d in dir:
            list_all.append([os.path.join(root, name) for root, dirs, files in os.walk(d) for name in dirs + files])
    
        flat_list = []
        for sublist in list_all:
            for item in sublist:
                flat_list.append(item)
        list_all = flat_list
    # Listing files
    list_files = [path for path in list_all if os.path.isfile(path) == True]

    # Handling extensions
    output = []
    if ext != None and isinstance(ext, str):
        output = [elt for elt in list_files if "." + ext in os.path.basename(elt)] # not working properly, need exact match.
        if len(output) == 0:
            print("No files found.\n" + "Please check the 'ext' argument.\n")
    elif ext != None:
        for e in ext:
            output.append([elt for elt in list_files if "." + ext in os.path.basename(elt)])
        print("Please check the 'ext' argument again.\n" + "It should be of type str.\n\n" + "Exiting now...\n")
        exit()
    else:
        output = list_files

    # Define and return output
    if all == True:
        output = tuple(list_all)
    else:
        output = tuple(output)

    return(output)


### EXAMPLES ------------------------------------------------

st_time = dt.datetime.now()
lfiles = get_file_information(dir_path=("/home/pi/Documents/", "/home/pi/Downloads/"), ext=None)
print(lfiles.unique_dirs)
print(len(lfiles.basename))
end_time = dt.datetime.now()
print(st_time)
print((end_time - st_time))
print(dt.datetime.utcfromtimestamp(lfiles.infos[1000].st_mtime))
